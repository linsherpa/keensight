from Image_normalization_4 import ImageNormalization
from Label_acquisition_1 import ImageLabels
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.optimizers import Adam, SGD
import time
import os

# changing to working directory
os.path.dirname(os.getcwd())
main_path = os.getcwd()
os.chdir(main_path)
directory = "keen_Demo_model"

class RunModel:
    def __init__(self):
        self.batch_size = None
        self.epochs = None
        self.ImageShape = ImageLabels().info()
        self.Input_sizeX = self.ImageShape[0]
        self.Input_sizeY = self.ImageShape[1]
        self.Imagedata = ImageNormalization().ImgNorm()
        self.data = self.Imagedata[0]
        self.labels = self.Imagedata[1]
        self.train_model = []
        self.train_model_time = None

    def RunCustomModel(self, batch_size, epochs):
        tf.random.set_seed(42)

        model = keras.Sequential([
            keras.layers.Conv2D(32, (3, 3), activation='relu', input_shape=(self.Input_sizeY, self.Input_sizeX, 3)),
            keras.layers.Conv2D(32, (3, 3), activation='relu'),
            keras.layers.MaxPooling2D(2, 2),
            keras.layers.Dropout(0.2),

            keras.layers.Conv2D(64, (3, 3), activation='relu'),
            keras.layers.Conv2D(64, (3, 3), activation='relu'),
            keras.layers.MaxPooling2D(2, 2),
            keras.layers.Dropout(0.2),

            keras.layers.Conv2D(256, (3, 3), activation='relu'),
            keras.layers.Conv2D(256, (3, 3), activation='relu'),
            keras.layers.MaxPooling2D(2, 2),
            keras.layers.Dropout(0.2),

            keras.layers.Flatten(),
            keras.layers.Dense(1024, activation='relu'),
            keras.layers.Dropout(0.5),
            keras.layers.Dense(3, activation='softmax')
        ])

        model.compile(optimizer=Adam(learning_rate=1e-05, beta_1=0.9,beta_2=0.999,epsilon=1e-08), loss='categorical_crossentropy', metrics=['accuracy'])
        #model.compile(optimizer=SGD(learning_rate=1e-08),loss='categorical_crossentropy', metrics=['accuracy'])
               

        # See an overview of the model architecture and to debug issues related to the model layers.
        model.summary()

        path = os.path.join(main_path,directory)

        if os.path.isdir(path):
            pass
        else:
            os.mkdir(path)
            
        os.chdir(path)
        model.save("OctModel_v1.0.h5")
        os.chdir(main_path)

        start_time = time.time()  # To show the training time

        # Train the model

        # set an early stopping mechanism
        # set patience to be tolerant against random validation loss increases
        early_stopping = tf.keras.callbacks.EarlyStopping(patience=5)

        # history = model.fit(data, labels_one_hot, batch_size=32, epochs=10, validation_split=0.2)
        history = model.fit(x=self.data,
                            y=self.labels,
                            batch_size=batch_size,
                            epochs=epochs,
                            validation_split=0.2,
                            shuffle=True,
                            callbacks=[early_stopping])

        # Evaluate the model
        print("Test accuracy: ", max(history.history['val_accuracy']))

        # Assign the trained model
        self.train_model = history

        end_time = time.time()  # To show the training time
        training_time = end_time - start_time
        print("Training time:", training_time, "seconds")

        self.train_model_time = training_time

        return self.train_model, self.train_model_time
