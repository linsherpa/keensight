from CNNModel_5 import RunModel


class TrainModel:
    def __init__(self, batch_size=25, epochs=200):
        self.train_model = []
        self.train_model_time = None
        self.batch_size = batch_size
        self.epochs = epochs

    def Train_Model(self):
        self.train_model, self.train_model_time = RunModel().RunCustomModel(
            batch_size=self.batch_size, epochs=self.epochs)
        return self.train_model, self.train_model_time
