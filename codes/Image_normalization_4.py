from Data_Splitting_3 import TrainTest
import numpy as np
from keras.utils import np_utils

class ImageNormalization:
    def __init__(self):
        self.imagedata = TrainTest().info()
        self.data = self.imagedata[0]
        self.labels = self.imagedata[1]
        self.X_test = TrainTest().splitting()[1]
        self.labels_encoded = []


    def ImgNorm1(self):
        self.data = self.data / 255.0
        self.X_test = self.X_test / 255.0

        self.labels_encoded = np.zeros((self.labels.shape[0], 3))
        for i, label in enumerate(self.labels):
            if label == "Low":
                self.labels_encoded[i, 0] = 1
            elif label == "Medium":
                self.labels_encoded[i, 1] = 1
            else:
                self.labels_encoded[i, 2] = 1

            # Show converted output
            #print(self.labels_encoded[2])

        return self.data, self.labels_encoded, self.X_test


    def ImgNorm(self):
        self.data = self.data / 255.0
        self.X_test = self.X_test / 255.0

        self.labels_encoded = np.zeros((self.labels.shape[0]))
        for i in range(len(self.labels)):
            if self.labels[i] == "Low":
                self.labels_encoded[i] = 0
            elif self.labels[i] == "Medium":
                self.labels_encoded[i] = 1
            else:
                self.labels_encoded[i] = 2

            # Show converted output
            #print(self.labels_encoded[2])
        self.labels_encoded = np_utils.to_categorical(self.labels_encoded)

        return self.data, self.labels_encoded, self.X_test
