from Image_normalization_4 import ImageNormalization
from Data_Splitting_3 import TrainTest
import numpy as np
from sklearn.metrics import accuracy_score
from keras.models import load_model
import os

# changing_path
os.path.dirname(os.getcwd())
main_path = os.getcwd()
os.chdir(main_path)
directory = "keen_Demo_model"


class ModelPrediction:
    def __init__(self):
        self.Y_true = []
        self.Y_pred = []
        self.X_test = ImageNormalization().ImgNorm()[2]
        self.Y_test = TrainTest().splitting()[3]
        self.model = "OctModel_v1.0.h5"
        self.predicted_labels = []
        self.accuracy = None

    def output_converter(self, model_output):
        output = model_output

        # assume that 'output' is a numpy array of shape (n, 3)
        output_labels = ['Low', 'Medium', 'High']
        predictions = np.argmax(output, axis=1)
        self.predicted_labels = [output_labels[p] for p in predictions]

        return self.predicted_labels

    def generate_prediction(self, model, name):
        path = os.path.join(main_path, directory)
        os.chdir(path)
        history = load_model(self.model)
        # Load output data
        #y_pred = self.output_converter(history.model.predict(self.X_test))
        y_pred = self.output_converter(history.predict(self.X_test))
        y_true = self.Y_test
        self.accuracy = accuracy_score(y_true, y_pred)
        print("{} accuracy score: {}".format(name, self.accuracy))
        with open("prediction.txt", "w") as file:
            file.write(str(self.accuracy))
        os.chdir(main_path)
        print(f'Vale of Y_pred: {y_pred}')
        return self.accuracy

    def predict_model(self):
        return self.generate_prediction(self.model, 'self trained CNNs')
