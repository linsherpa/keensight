# (Work in progress)
# KEENsight 
<div align="center">
![Alt text](images/synergy.gif)

An Open-source, scalable, adaptive, and inteoperable MLOps environment. Powered by open-source tools.
</div>


# Motivation
<div align="center">
![Alt text](images/distribution.gif)
</div>

Due to the diverse needs and preferences of different organizations, the evolving landscape of machine learning, and the specific challenges faced by academia, such as interdisciplinary collaborations with steep learning curves, diverse computing resources and their management, lack of standardized scientific data management, and various regulations and policies, the development of another MLOps pipeline was indeed necessary.

# Architecture
KEENsight's architecture enables multifaceted collaborations despite the diversity of underlying solutions.
![Alt text](images/architecture.png)

# Proof-of-Concept
<div align="center">
![Alt text](images/poc.gif)
</div>