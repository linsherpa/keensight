From ubuntu:22.04
RUN apt-get update && \
    apt-get -qy full-upgrade && \
    apt-get -y install systemd && \
    apt-get install -qy curl && \
    apt-get install -y nodejs && \
    apt-get install -y docker.io && \
    apt-get install -y python3-pip && \
    pip3 install opencv-python==4.7.0.72 && \
    pip3 install numpy && \
    pip3 install pandas==2.1.1 && \
    pip3 install matplotlib && \
    pip3 install tensorflow==2.10.0 && \
    pip3 install scikit-learn==1.3.2 && \
    pip3 install seaborn==0.13.0 && \
    pip3 install keras==2.10.0 && \
    apt-get install -y ffmpeg && \
    apt-get install -y libsm6 && \
    apt-get install -y wget && \
    apt-get install -y unzip && \
    apt-get install -y libxext6 
